SET( ${TARGETLIB}_WRAP_UI
)

SET( ${TARGETLIB}_SOURCES
	YNCWE.cc
	YMGA_NCCBTable.cc
	YMGANCWidgetFactory.cc
)

SET( ${TARGETLIB}_HEADERS
  ##### Here go the headers
  YNCWE.h
  YMGA_NCCBTable.h
  YMGANCWidgetFactory.h
)

SET( EXAMPLES_LIST
  ##### Here go the examples
)

